// lab 1
/*
- input:
  + Ba số tự nhiên ngẫu nhiên
- process:
  + Gán giá trị num1, num2, num3
  + So sánh các giá trị tìm ra min, middle, max
- output:
  + Thứ tự tăng dần của ba số min, middle, max
 */
function sortNum() {
  var num1 = document.getElementById("num1").value * 1;
  var num2 = document.getElementById("num2").value * 1;
  var num3 = document.getElementById("num3").value * 1;
  var min, middle, max;

  if (num1 >= num2 && num1 >= num3) {
    max = num1;
    if (num2 > num3) {
      middle = num2;
      min = num3;
    } else {
      middle = num3;
      min = num2;
    }
  } else if (num2 >= num1 && num2 >= num3) {
    max = num2;
    if (num1 > num3) {
      middle = num1;
      min = num3;
    } else {
      middle = num3;
      min = num1;
    }
  } else if (num3 >= num1 && num3 >= num2) {
    max = num3;
    if (num1 > num2) {
      middle = num1;
      min = num2;
    } else {
      middle = num2;
      min = num1;
    }
  }
  document.getElementById(
    "result"
  ).innerHTML = `<span>${min}, ${middle}, ${max}</span>`;
}
// lab 2
/*
- input:
  + Tên thành viên
- process:
  - So sánh các value lấy từ thẻ option bằng switch case
  - Gán kết quả vào biến answer
- output:
  + In lời chào hỏi ra màn hình
 */
function greetings() {
  var check = document.getElementById("thanhVien").value;
  var answer = "";
  switch (check) {
    case "B":
      answer = "Bố!";
      break;
    case "M":
      answer = "Mẹ!";
      break;
    case "A":
      answer = "Anh Trai!";
      break;
    case "E":
      answer = "Em Gái!";
      break;
    default:
      answer = "Người lạ ơi!";
      break;
  }
  document.getElementById("loiChao").innerHTML = `<span>${answer}</span>`;
}
// lab 3
/*
- input:
  + Ba số tự nhiên ngẫu nhiên
- process:
  + Tạo các biến num1, num2, num3, temp, result
  + Gán giá trị cho biến num1, num2, num3
  + Sử dụng điều kiện để tìm ra số lượng sô chẵn temp
  + Từ đó tìm ra số lượng số lẻ result
- output:
  + In kết quả ra màn hình
 */
function checkNum() {
  var num1 = document.getElementById("number1").value * 1;
  var num2 = document.getElementById("number2").value * 1;
  var num3 = document.getElementById("number3").value * 1;
  var temp = 0; // so luong so chan
  var result = 0; // so luong so le

  if (num1 <= 0 || num2 <= 0 || num3 <= 0) {
    alert("Nhập không hợp lệ");
  } else {
    if (num1 % 2 == 0) {
      temp++;
    }
    if (num2 % 2 == 0) {
      temp++;
    }
    if (num3 % 2 == 0) {
      temp++;
    }
  }
  result = 3 - temp;
  document.getElementById(
    "dem"
  ).innerHTML = `<span>Số lượng số chẵn: ${temp}<br />số lượng số lẻ: ${result}</span>`;
}
// lab 4
/*
- input:
  + Giá trị của các cạnh của tam giác
- process:
  + Dùng câu điều kiện kết hợp công thức để so sánh và tìm ra loại tam giác
- Output:
  + In kết quả ra màn hình
 */
function checkTriangle() {
  var a = document.getElementById("canh1").value * 1;
  var b = document.getElementById("canh2").value * 1;
  var c = document.getElementById("canh3").value * 1;
  var result = "";

  if (a + b > c && a + c > b && b + c > a) {
    if (a == b || a == c || b == c) {
      if (a != b || a != c || b != c) {
        result = "Tam giác cân";
      } else {
        result = "Tam giác đều";
      }
    } else {
      result = "Một loại tam giác khác";
    }
    if (
      Math.pow(a, 2) + Math.pow(b, 2) == Math.pow(c, 2) ||
      Math.pow(a, 2) + Math.pow(c, 2) == Math.pow(b, 2) ||
      Math.pow(b, 2) + Math.pow(c, 2) == Math.pow(a, 2)
    ) {
      result = "Tam giác vuông";
    }
  } else {
    alert("Nhập không hợp lệ");
  }
  document.getElementById("duDoan").innerHTML = `<span>${result}</span>`;
}
// lab 5
/*
- input:
  + Ngày, tháng và năm
- process:
  + Gán các trị số ngày của các tháng tương ứng
  + Kiểm tra năm nhuận hay không phải năm nhuận để biết tháng 2 có 29 hay 28 ngày
  + Đối với ngày tiếp theo thì kiểm tra thêm ngày cuối tháng
  + Đối với ngày hôm qua thì kiểm tra thêm ngày đầu tháng
- output:
  + In ra ngày tiếp theo và ngày hôm qua
 */
const MONTH_31_DAY = 31;
const MONTH_28_DAY = 28;
const MONTH_30_DAY = 30;

var btnNgayTiepTheo = document.getElementById("btnNgayTiepTheo");
var btnNgayTruocDo = document.getElementById("btnNgayTruocDo");

function checkNamNhuan(year) {
  if (year % 4 == 0) {
    if (year % 100 == 0) {
      if (year % 400 == 0) {
        return "Nhuận";
      } else {
        return "Không Nhuận";
      }
    } else {
      return "Nhuận";
    }
  } else {
    return "Không Nhuận";
  }
}

btnNgayTiepTheo.addEventListener("click", function () {
  var day = document.getElementById("day").value * 1;
  var month = document.getElementById("month").value * 1;
  var year = document.getElementById("year").value * 1;

  var kiemTraNamNhuan = checkNamNhuan(year);
  var day1 = "";
  var month1 = "";
  var year1 = "";
  var ngayTiepTheo = "";

  if (year >= 1920) {
    if (day > 0 && day <= 31 && month > 0 && month <= 12 && year > 0) {
      if (kiemTraNamNhuan == "Nhuận") {
        if (month == 2 && day <= MONTH_28_DAY + 1) {
          if (day == MONTH_28_DAY + 1) {
            day1 = day = 1;
            month1 = month + 1;
            year1 = year;
            ngayTiepTheo = day1 + "/" + month1 + "/" + year1;
          } else {
            day1 = day + 1;
            month1 = month;
            year1 = year;
            ngayTiepTheo = day1 + "/" + month1 + "/" + year1;
          }
        } else {
          ngayTiepTheo =
            "Nhập không hợp lệ vì năm Nhuận tháng 2 chỉ có 29 ngày";
        }
        if (month == 12) {
          if (day == MONTH_31_DAY) {
            day1 = day = 1;
            month1 = 1;
            year1 = year + 1;
            ngayTiepTheo = day1 + "/" + month1 + "/" + year1;
          } else {
            day1 = day + 1;
            month1 = month;
            year1 = year;
            ngayTiepTheo = day1 + "/" + month1 + "/" + year1;
          }
        }
        if (day <= MONTH_30_DAY) {
          if (month == 4 || month == 6 || month == 9 || month == 11) {
            if (day == MONTH_30_DAY) {
              day1 = day = 1;
              month1 = month + 1;
              year1 = year;
              ngayTiepTheo = day1 + "/" + month1 + "/" + year1;
            } else {
              day1 = day + 1;
              month1 = month;
              year1 = year;
              ngayTiepTheo = day1 + "/" + month1 + "/" + year1;
            }
          }
        } else {
          ngayTiepTheo = "Nhập không hợp lệ";
        }
        if (
          month == 1 ||
          month == 3 ||
          month == 5 ||
          month == 7 ||
          month == 8 ||
          month == 10
        ) {
          if (day == MONTH_31_DAY) {
            day1 = day = 1;
            month1 = month + 1;
            year1 = year;
            ngayTiepTheo = day1 + "/" + month1 + "/" + year1;
          } else {
            day1 = day + 1;
            month1 = month;
            year1 = year;
            ngayTiepTheo = day1 + "/" + month1 + "/" + year1;
          }
        }
      } else {
        if (day <= MONTH_28_DAY) {
          if (month == 2) {
            if (day == MONTH_28_DAY) {
              day1 = day = 1;
              month1 = month + 1;
              year1 = year;
              ngayTiepTheo = day1 + "/" + month1 + "/" + year1;
            } else {
              day1 = day + 1;
              month1 = month;
              year1 = year;
              ngayTiepTheo = day1 + "/" + month1 + "/" + year1;
              console.log(ngayTiepTheo);
            }
          }
        } else {
          ngayTiepTheo = "Nhập không hợp lệ vì tháng 2 chỉ có 28 ngày";
        }
        if (month == 12) {
          if (day == MONTH_31_DAY) {
            day1 = day = 1;
            month1 = 1;
            year1 = year + 1;
            ngayTiepTheo = day1 + "/" + month1 + "/" + year1;
          } else {
            day1 = day + 1;
            month1 = month;
            year1 = year;
            ngayTiepTheo = day1 + "/" + month1 + "/" + year1;
          }
        }
        if (day <= MONTH_30_DAY) {
          if (month == 4 || month == 6 || month == 9 || month == 11) {
            if (day == MONTH_30_DAY) {
              day1 = day = 1;
              month1 = month + 1;
              year1 = year;
              ngayTiepTheo = day1 + "/" + month1 + "/" + year1;
            } else {
              day1 = day + 1;
              month1 = month;
              year1 = year;
              ngayTiepTheo = day1 + "/" + month1 + "/" + year1;
            }
          }
        } else {
          ngayTiepTheo = "Nhập không hợp lệ";
        }
        if (
          month == 1 ||
          month == 3 ||
          month == 5 ||
          month == 7 ||
          month == 8 ||
          month == 10
        ) {
          if (day == MONTH_31_DAY) {
            day1 = day = 1;
            month1 = month + 1;
            year1 = year;
            ngayTiepTheo = day1 + "/" + month1 + "/" + year1;
          } else {
            day1 = day + 1;
            month1 = month;
            year1 = year;
            ngayTiepTheo = day1 + "/" + month1 + "/" + year1;
          }
        }
      }
    } else {
      alert("Nhập không hợp lệ");
    }
  } else {
    alert("Năm cần lớn hơn 1920");
  }
  document.getElementById("ngayThangNam").innerHTML = ngayTiepTheo;
});

btnNgayTruocDo.addEventListener("click", function () {
  var day = document.getElementById("day").value * 1;
  var month = document.getElementById("month").value * 1;
  var year = document.getElementById("year").value * 1;

  var kiemTraNamNhuan = checkNamNhuan(year);
  var day1 = "";
  var month1 = "";
  var year1 = "";
  var ngayTruocDo = "";

  if (year >= 1920) {
    if (day > 0 && day <= 31 && month > 0 && month <= 12 && year > 0) {
      if (kiemTraNamNhuan == "Nhuận") {
        if (month == 2 && day <= MONTH_28_DAY + 1) {
          if (day == 1) {
            day1 = day = MONTH_31_DAY;
            month1 = month - 1;
            year1 = year;
            ngayTruocDo = day1 + "/" + month1 + "/" + year1;
          } else {
            day1 = day - 1;
            month1 = month;
            year1 = year;
            ngayTruocDo = day1 + "/" + month1 + "/" + year1;
          }
        } else {
          ngayTruocDo = "Nhập không hợp lệ vì năm Nhuận tháng 2 chỉ có 29 ngày";
        }
        if (day <= MONTH_30_DAY) {
          if (month == 4 || month == 6 || month == 9 || month == 11) {
            if (day == 1) {
              day1 = day = MONTH_31_DAY;
              month1 = month - 1;
              year1 = year;
              ngayTruocDo = day1 + "/" + month1 + "/" + year1;
            } else {
              day1 = day - 1;
              month1 = month;
              year1 = year;
              ngayTruocDo = day1 + "/" + month1 + "/" + year1;
            }
          }
        } else {
          // ngayTruocDo = "Nhập không hợp lệ";
        }
        if (month == 1) {
          if (day == 1) {
            day1 = day = MONTH_31_DAY;
            month1 = 12;
            year1 = year - 1;
            ngayTruocDo = day1 + "/" + month1 + "/" + year1;
          } else {
            day1 = day - 1;
            month1 = month;
            year1 = year;
            ngayTruocDo = day1 + "/" + month1 + "/" + year1;
          }
        }
        if (
          month == 12 ||
          month == 5 ||
          month == 7 ||
          month == 10 ||
          month == 8
        ) {
          if (day == 1) {
            day1 = day = MONTH_30_DAY;
            month1 = month - 1;
            year1 = year;
            ngayTruocDo = day1 + "/" + month1 + "/" + year1;
          } else {
            day1 = day - 1;
            month1 = month;
            year1 = year;
            ngayTruocDo = day1 + "/" + month1 + "/" + year1;
          }
        }
        if (month == 3) {
          if (day == 1) {
            day1 = 29;
            month1 = month - 1;
            year1 = year;
            ngayTruocDo = day1 + "/" + month1 + "/" + year1;
          } else {
            day1 = day - 1;
            month1 = month;
            year1 = year;
            ngayTruocDo = day1 + "/" + month1 + "/" + year1;
          }
        }
      } else {
        if (month == 2 && day <= MONTH_28_DAY) {
          if (day == 1) {
            day1 = day = MONTH_31_DAY;
            month1 = month - 1;
            year1 = year;
            ngayTruocDo = day1 + "/" + month1 + "/" + year1;
            console.log(ngayTruocDo);
          } else {
            day1 = day - 1;
            month1 = month;
            year1 = year;
            ngayTruocDo = day1 + "/" + month1 + "/" + year1;
            console.log(ngayTruocDo);
          }
        } else {
          ngayTruocDo = "Nhập không hợp lệ vì năm Nhuận tháng 2 chi có 28 ngày";
        }
        if (day <= MONTH_30_DAY) {
          if (month == 4 || month == 6 || month == 9 || month == 11) {
            if (day == 1) {
              day1 = day = MONTH_31_DAY;
              month1 = month - 1;
              year1 = year;
              ngayTruocDo = day1 + "/" + month1 + "/" + year1;
            } else {
              day1 = day - 1;
              month1 = month;
              year1 = year;
              ngayTruocDo = day1 + "/" + month1 + "/" + year1;
            }
          }
        } else {
          // ngayTruocDo = "Nhập không hợp lệ";
        }
        if (month == 1) {
          if (day == 1) {
            day1 = day = MONTH_31_DAY;
            month1 = 12;
            year1 = year - 1;
            ngayTruocDo = day1 + "/" + month1 + "/" + year1;
          } else {
            day1 = day - 1;
            month1 = month;
            year1 = year;
            ngayTruocDo = day1 + "/" + month1 + "/" + year1;
          }
        }
        if (
          month == 12 ||
          month == 5 ||
          month == 7 ||
          month == 8 ||
          month == 10
        ) {
          if (day == 1) {
            day1 = day = MONTH_30_DAY;
            month1 = month - 1;
            year1 = year;
            ngayTruocDo = day1 + "/" + month1 + "/" + year1;
          } else {
            day1 = day - 1;
            month1 = month;
            year1 = year;
            ngayTruocDo = day1 + "/" + month1 + "/" + year1;
          }
        }
        if (month == 3) {
          if (day == 1) {
            day1 = day = MONTH_28_DAY;
            month1 = month - 1;
            year1 = year;
            ngayTruocDo = day1 + "/" + month1 + "/" + year1;
          } else {
            day1 = day - 1;
            month1 = month;
            year1 = year;
            ngayTruocDo = day1 + "/" + month1 + "/" + year1;
          }
        }
      }
    } else {
      alert("Nhập không hợp lệ");
    }
  } else {
    alert("Năm cần lớn hơn 1920");
  }
  document.getElementById("ngayThangNam").innerHTML = ngayTruocDo;
});
// lab 6
/*
- input:
  + Tháng và năm
- process:
  + Gán giá trị số ngày của các tháng tương ứng
  + Kiểm tra có phải là năm nhuận hay không
  + So sánh nếu năm nhuận thì tháng 2 có 29 ngày, ngược lại tháng 2 có 28 ngày
- output:
  + In ra số ngày tương ứng với tháng người dùng nhập vào
 */
var btnTinhNgay = (document.getElementById("tinhNgay").onclick = function () {
  var thang = document.getElementById("txt-thang").value * 1;
  var nam = document.getElementById("txt-nam").value * 1;
  var ngay = 0;
  var error = "";

  if (nam >= 1920) {
    switch (thang) {
      case 1:
      case 3:
      case 5:
      case 7:
      case 8:
      case 10:
      case 12:
        ngay = MONTH_31_DAY;
        break;
      case 4:
      case 6:
      case 9:
      case 11:
        ngay = MONTH_30_DAY;
        break;
      case 2:
        if (checkNamNhuan(nam) == "Nhuận") {
          ngay = MONTH_28_DAY + 1;
        } else {
          ngay = MONTH_28_DAY;
        }
        break;
      default:
        alert("Nhập tháng không hợp lệ");
        break;
    }
  } else {
    alert("Năm phải lớn hơn 1920");
  }
  document.getElementById(
    "soNgay"
  ).innerHTML = `<span>Tháng ${thang} năm ${nam} có ${ngay} ngày</span>`;
});
// lab 7
/*
- input:
  + Số tự nhiên có 3 chữ số ngẫu nhiên
- process:
  + Tìm ra số hàng trăm, hàng chục và hàng đơn vị
  + Viết hàm để chuyển đổi số sang chữ
  + Dùng câu điều kiện để so sánh các trường hợp
- Output:
  + In kết quả ra màn hình
 */
function doiChu(so) {
  switch (so) {
    case 1:
      return "một";
    case 2:
      return "hai";
    case 3:
      return "ba";
    case 4:
      return "bốn";
    case 5:
      return "năm";
    case 6:
      return "sáu";
    case 7:
      return "bảy";
    case 8:
      return "tám";
    case 9:
      return "chín";

    default:
      break;
  }
}
var btnDocSo = (document.getElementById("btnDocSo").onclick = function () {
  var nhapSo = document.getElementById("txt-nhap-so").value * 1;

  var hangTram = Math.floor(nhapSo / 100);
  console.log(hangTram);
  var hangChuc = Math.floor((nhapSo / 10) % 10);
  console.log(hangChuc);
  var hangDonVi = nhapSo % 10;
  console.log(hangDonVi);

  var hangTram1 = doiChu(hangTram);
  var hangChuc1 = doiChu(hangChuc);
  var hangDonVi1 = doiChu(hangDonVi);

  if (nhapSo >= 100) {
    if (nhapSo % 100 == 0) {
      document.getElementById("kqDocSo").innerHTML = `${hangTram1} trăm`;
    } else if (hangChuc == 0) {
      document.getElementById(
        "kqDocSo"
      ).innerHTML = `${hangTram1} trăm lẻ ${hangDonVi1}`;
    } else if (hangDonVi == 0) {
      if (hangChuc == 1) {
        document.getElementById("kqDocSo").innerHTML = `${hangTram1} trăm mười`;
      } else {
        document.getElementById(
          "kqDocSo"
        ).innerHTML = `${hangTram1} trăm ${hangChuc1} mươi`;
      }
    } else {
      if (hangChuc == 1) {
        document.getElementById(
          "kqDocSo"
        ).innerHTML = `${hangTram1} trăm ${hangChuc1} mười ${hangDonVi1}`;
      } else {
        document.getElementById(
          "kqDocSo"
        ).innerHTML = `${hangTram1} trăm ${hangChuc1} mươi ${hangDonVi1}`;
      }
    }
  } else {
    alert("Nhập không hợp lệ");
  }
});
// lab 8
/*
- input:
  + Tên sinh viên 1, sinh viên 2, sinh viên 3, tọa độ x y sinh viên 1, tọa độ x y sinh viên 2, tọa độ x y sinh viên 3, tọa độ trường học
- process:
  + Tính quãng đường từ nhà của từng sinh viên tới trường
  + So sánh quãng đường để tìm ra nhà sinh viên nào xa trường nhất
- output:
  + In tên sinh viên xa trường nhất
 */
var btnTim = (document.getElementById("btnTim").onclick = function () {
  var ten1 = document.getElementById("ten1").value;
  var ten2 = document.getElementById("ten2").value;
  var ten3 = document.getElementById("ten3").value;

  var toaDoX1 = document.getElementById("toaDoX1").value * 1;
  var toaDoX2 = document.getElementById("toaDoX2").value * 1;
  var toaDoX3 = document.getElementById("toaDoX3").value * 1;
  var toaDoX4 = document.getElementById("toaDoX4").value * 1;

  var toaDoY1 = document.getElementById("toaDoY1").value * 1;
  var toaDoY2 = document.getElementById("toaDoY2").value * 1;
  var toaDoY3 = document.getElementById("toaDoY3").value * 1;
  var toaDoY4 = document.getElementById("toaDoY4").value * 1;

  var d1 = Math.sqrt(
    Math.pow(toaDoX4 - toaDoX1, 2) + Math.pow(toaDoY4 - toaDoY1, 2)
  );
  var d2 = Math.sqrt(
    Math.pow(toaDoX4 - toaDoX2, 2) + Math.pow(toaDoY4 - toaDoY2, 2)
  );
  var d3 = Math.sqrt(
    Math.pow(toaDoX4 - toaDoX3, 2) + Math.pow(toaDoY4 - toaDoY3, 2)
  );
  var ketQuad = "";
  if (
    ten1 == "" ||
    ten2 == "" ||
    ten3 == "" ||
    toaDoX1 == "" ||
    toaDoX2 == "" ||
    toaDoX3 == "" ||
    toaDoX4 == "" ||
    toaDoY1 == "" ||
    toaDoY2 == "" ||
    toaDoY3 == "" ||
    toaDoY4 == ""
  ) {
    alert("Nhập không hợp lệ");
  } else {
    if (d1 > d2 && d1 > d3) {
      ketQuad = "Sinh viên xa trường nhất là: " + ten1;
    } else if (d2 > d1 && d2 > d3) {
      ketQuad = "Sinh viên xa trường nhất là: " + ten2;
    } else {
      ketQuad = "Sinh viên xa trường nhất là: " + ten3;
    }
  }
  document.getElementById("kqBai8").innerHTML = ketQuad;
});
