const MONTH_31_DAY = 31;
const MONTH_28_DAY = 28;
const MONTH_30_DAY = 30;

var day = 1;
var month = 1;
var year = 2021;

// 1 3 5 7 8 10 12 == 31
// 2 == 28 || 2 == 29
// 4 6 9 11 == 30

function kiemTraNamNhuan(){
    if(year % 4 == 0){
        if (year % 100 == 0){
            if (year % 400 == 0){
                return "Nhuận";
            }else {
                return "Không Nhuận";
            }
        }else {
            return "Nhuận";
        }
    }else {
        return "Không Nhuận";
    }
}
console.log(kiemTraNamNhuan());

function ngayTiepTheo(){
    if (day > 0 && day <= 31 && month > 0 && month <= 12 && year > 0){
        if(kiemTraNamNhuan() == "Nhuận"){
            if (month == 4 || month == 6 || month == 9 || month == 11){
                if(day == MONTH_30_DAY){
                    day = 1;
                    month = month + 1;
                    year = year
                    // console.log(day + "/" + month + "/" + year);
                    return;
                }else {
                    day = day + 1;
                    month = month;
                    year = year;
                    // console.log(day + "/" + month + "/" + year);
                }
            }
            if(month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10){
                if(day == MONTH_31_DAY){
                    day = 1;
                    month = month + 1;
                    year = year;
                    // console.log(day + "/" + month + "/" + year);
                    return;
                }else {
                    day = day + 1;
                    month = month;
                    year = year;
                    // console.log(day + "/" + month + "/" + year);
                }
            }
            if(month == 12){
                if(day == MONTH_31_DAY){
                    day = 1;
                    month = 1;
                    year = year + 1;
                    // console.log(day + "/" + month + "/" + year);
                }else {
                    day = day + 1;
                    month = month;
                    year = year;
                    // console.log(day + "/" + month + "/" + year);
                }
            }
            if(month == 2 && day <= MONTH_28_DAY + 1){
                if(day == MONTH_28_DAY + 1){
                    day = 1;
                    month = month + 1;
                    year = year;
                    // console.log(day + "/" + month + "/" + year);
                }else {
                    day = day + 1;
                    month = month;
                    year = year;
                    // console.log(day + "/" + month + "/" + year);
                }
            }
        }else {
            if (month == 4 || month == 6 || month == 9 || month == 11){
                if(day == 30){
                    day = 1;
                    month = month + 1;
                    year = year
                }else {
                    day = day + 1;
                    month = month;
                    year = year;
                    // console.log(day + "/" + month + "/" + year);
                }
            }
            if(month == 1 || month == 3 || month == 5 || month == 7 || month ==8 || month == 10){
                if(day == MONTH_31_DAY){
                    day = 1;
                    month = month + 1;
                    year = year;
                    // console.log(day + "/" + month + "/" + year);
                    return;
                }else {
                    day = day + 1;
                    month = month;
                    year = year;
                    // console.log(day + "/" + month + "/" + year);
                }
            }
            if(month == 12){
                if(day == MONTH_31_DAY){
                    day = 1;
                    month = 1;
                    year = year + 1;
                    // console.log(day + "/" + month + "/" + year);
                }else {
                    day = day + 1;
                    month = month;
                    year = year;
                    // console.log(day + "/" + month + "/" + year);
                }
            }
            if(month == 2 && day <= MONTH_28_DAY){
                if(day == MONTH_28_DAY){
                    day = 1;
                    month = month + 1;
                    year = year;
                    // console.log(day + "/" + month + "/" + year);
                }else {
                    day = day + 1;
                    month = month;
                    year = year;
                    // console.log(day + "/" + month + "/" + year);
                }
            }
        }
    }else {
        console.log("Nhập không hợp lệ");
    }
    console.log(day + "/" + month + "/" + year);
}
// console.log("ngay tiep theo");
// ngayTiepTheo();

function ngayTruocDo(){
    if (day > 0 && day <= 31 && month > 0 && month <= 12 && year > 0){
        if(kiemTraNamNhuan() == "Nhuận"){
            if(month == 1){
                if(day == 1){
                    day = MONTH_31_DAY;
                    month = 12;
                    year = year - 1;
                    // console.log(day + "/" + month + "/" + year);
                }else {
                    day = day - 1;
                    month = month;
                    year = year;
                    // console.log(day + "/" + month + "/" + year);
                }
            }
            if (month == 4 || month == 6 || month == 9 || month == 11){
                if(day == 1){
                    day = MONTH_31_DAY;
                    month = month - 1;
                    year = year;
                    console.log(day + "/" + month + "/" + year);
                    return;
                }else {
                    day = day - 1;
                    month = month;
                    year = year;
                    // console.log(day + "/" + month + "/" + year);
                }
            }
            if(month == 12 || month == 5 || month == 7 || month ==8 || month == 10){
                if(day == 1){
                    day = MONTH_30_DAY;
                    month = month - 1;
                    year = year;
                    console.log(day + "/" + month + "/" + year);
                    return;
                }else {
                    day = day - 1;
                    month = month;
                    year = year;
                    // console.log(day + "/" + month + "/" + year);
                }
            }
            if(month == 2 && day <= MONTH_28_DAY + 1){
                if(day == MONTH_28_DAY + 1){
                    day = day - 1;
                    console.log(day)
                    month = month;
                    year = year;
                    // console.log(day + "/" + month + "/" + year);
                }else {
                    day = day - 1;
                    month = month;
                    year = year;
                    // console.log(day + "/" + month + "/" + year);
                }
            }
            if(month == 3){
                if(day == 1){
                    day = 29;
                    month = month - 1;
                    year = year;
                }else {
                    day = day - 1;
                    month = month;
                    year = year;
                }
            }
        }else {
            if(month == 1){
                if(day == 1){
                    day = MONTH_31_DAY;
                    month = 12;
                    year = year - 1;
                    // console.log(day + "/" + month + "/" + year);
                }else {
                    day = day - 1;
                    month = month;
                    year = year;
                    // console.log(day + "/" + month + "/" + year);
                }
            }
            if (month == 4 || month == 6 || month == 9 || month == 11){
                if(day == 1){
                    day = MONTH_31_DAY;
                    month = month - 1;
                    year = year;
                    console.log(day + "/" + month + "/" + year);
                    return;
                }else {
                    day = day - 1;
                    month = month;
                    year = year;
                    // console.log(day + "/" + month + "/" + year);
                }
            }
            if(month == 12 || month == 5 || month == 7 || month ==8 || month == 10){
                if(day == 1){
                    day = MONTH_30_DAY;
                    month = month - 1;
                    year = year;
                    console.log(day + "/" + month + "/" + year);
                    return;
                }else {
                    day = day - 1;
                    month = month;
                    year = year;
                    // console.log(day + "/" + month + "/" + year);
                }
            }
            if(month == 2 && day <= MONTH_28_DAY){
                if(day == MONTH_28_DAY){
                    day = day - 1;
                    month = month;
                    year = year;
                    // console.log(day + "/" + month + "/" + year);
                }else {
                    day = day - 1;
                    month = month;
                    year = year;
                    // console.log(day + "/" + month + "/" + year);
                }
            }
            if(month == 3){
                if(day == 1){
                    day = 28;
                    month = month - 1;
                    year = year;
                }else {
                    day = day - 1;
                    month = month;
                    year = year;
                }
            }
        }
    }else {
        console.log("Nhập không hợp lệ");
    }
    console.log(day + "/" + month + "/" + year);
}
console.log("ngay truoc do");
ngayTruocDo();